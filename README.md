# OddestOdds
Author: Nicholas Bonello

The project was split into two seperate solutions; 

- Frontend.sln : The frontend customer facing console application. Here, odds can be seen updated in real time. 
- OddsManager.sln: This is the backend service that admins can use to create/update/delete odds in realtime.  
# Project Description
The design was to follow the N-tier architecture, where frontend and backend services were split up. Within services, communications with databases, entities, and application services are split up in a similar fashion.

Given that the only communication that needed to take place was backend -> frontend, a single RabbitMQ queue was used to publish and consume messages. This was preferred to an API architecture due to the lack of 2 way communication.

The backend service stores fixtures on the database, and once the publish button is pressed all the updates are pushed to the queue and consumed by the frontend service.

# Prerequirements
- Visual Studio 2017
- .NET Core
- SQL Server
- RabbitMQ

# How To Run    
- Ensure that RabbitMQ is running locally.
    - username: guest 
    - password: guest 
    - vhost: /
- Ensure that MySQL server is running locally
- Build Frontend.sln and install all required packages.
    - Run Frontend.sln. Will automatically create the required exchange and queue
- Build OddsManager.sln and install all required packages.
    - Run OddsManager.sln. Will automatically connect to exchange, create SQL table and install all required packages. 
- Follow the instructions on the OddsManager program.

# Tests

Both projects contain their own set of tests that ensure all the required functionality works as required - found under Frontend.Tests and OddestOdds.Tests projects respectively
