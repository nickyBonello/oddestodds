﻿using System;
using System.Text;
using Frontend.Application;
using Frontend.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace Frontend.Tests
{
    [TestClass]
    public class MessageReceiverTests
    {
        [TestMethod]
        public void CreateMessageTest()
        {
            var model = new Mock<IModel>();
            var messageHandler = new MessageHandler();
            var messageReceiver = new MessageReceiver(model.Object, messageHandler);

            var odd = JsonConvert.SerializeObject(GenerateMockOdd());

            messageReceiver.HandleBasicDeliver(It.IsAny<string>(), It.IsAny<ulong>(), false, It.IsAny<string>(), "create",
                It.IsAny<IBasicProperties>(), Encoding.UTF8.GetBytes(odd));

            Assert.IsTrue(messageHandler.AllOdds.Count == 1);
        }

        [TestMethod]
        public void UpdateBeforeCreateTest()
        {
            var model = new Mock<IModel>();
            var messageHandler = new MessageHandler();
            var messageReceiver = new MessageReceiver(model.Object, messageHandler);

            var odd = GenerateMockOdd();
            var jsonOdd = JsonConvert.SerializeObject(odd);

            messageReceiver.HandleBasicDeliver(It.IsAny<string>(), It.IsAny<ulong>(), false, It.IsAny<string>(), "create",
                It.IsAny<IBasicProperties>(), Encoding.UTF8.GetBytes(jsonOdd));

            Assert.IsTrue(messageHandler.AllOdds.Count == 1);

            //Try send update before odd was created on UI - should not add new odd
            var odd2 = GenerateMockOdd();
            var jsonOdd2 = JsonConvert.SerializeObject(odd2);

            messageReceiver.HandleBasicDeliver(It.IsAny<string>(), It.IsAny<ulong>(), false, It.IsAny<string>(), "update",
                It.IsAny<IBasicProperties>(), Encoding.UTF8.GetBytes(jsonOdd2));

            Assert.IsTrue(messageHandler.AllOdds.Count == 1);
        }

        [TestMethod]
        public void CreateAndUpdateTest()
        {
            var model = new Mock<IModel>();
            var messageHandler = new MessageHandler();
            var messageReceiver = new MessageReceiver(model.Object, messageHandler);

            var odd = GenerateMockOdd();
            var jsonOdd = JsonConvert.SerializeObject(odd);

            messageReceiver.HandleBasicDeliver(It.IsAny<string>(), It.IsAny<ulong>(), false, It.IsAny<string>(), "create",
                It.IsAny<IBasicProperties>(), Encoding.UTF8.GetBytes(jsonOdd));

            Assert.IsTrue(messageHandler.AllOdds.Count == 1);

            var odd2 = GenerateMockOdd();
            var jsonOdd2 = JsonConvert.SerializeObject(odd2);

            messageReceiver.HandleBasicDeliver(It.IsAny<string>(), It.IsAny<ulong>(), false, It.IsAny<string>(), "create",
                It.IsAny<IBasicProperties>(), Encoding.UTF8.GetBytes(jsonOdd2));

            Assert.IsTrue(messageHandler.AllOdds.Count == 2);

            odd2.HomeOdds = 5.44f;
            jsonOdd2 = JsonConvert.SerializeObject(odd2);

            messageReceiver.HandleBasicDeliver(It.IsAny<string>(), It.IsAny<ulong>(), false, It.IsAny<string>(), "update",
                It.IsAny<IBasicProperties>(), Encoding.UTF8.GetBytes(jsonOdd2));

            Assert.IsTrue(messageHandler.AllOdds.Count == 2);
            Assert.IsTrue(messageHandler.AllOdds[1].HomeOdds == 5.44f);
        }

        [TestMethod]
        public void DeleteMessageTest()
        {
            var model = new Mock<IModel>();
            var messageHandler = new MessageHandler();
            var messageReceiver = new MessageReceiver(model.Object, messageHandler);

            var odd = JsonConvert.SerializeObject(GenerateMockOdd());

            messageReceiver.HandleBasicDeliver(It.IsAny<string>(), It.IsAny<ulong>(), false, It.IsAny<string>(), "create",
                It.IsAny<IBasicProperties>(), Encoding.UTF8.GetBytes(odd));

            Assert.IsTrue(messageHandler.AllOdds.Count == 1);

            messageReceiver.HandleBasicDeliver(It.IsAny<string>(), It.IsAny<ulong>(), false, It.IsAny<string>(), "delete",
                It.IsAny<IBasicProperties>(), Encoding.UTF8.GetBytes("{}"));

            Assert.IsTrue(messageHandler.AllOdds.Count == 0);
        }



        private static Odd GenerateMockOdd()
        {
            return new Odd
            {
                FixtureId = $"F-{Guid.NewGuid()}",
                Name = $"Test1 vs Test2",
                HomeOdds = 3.23f,
                DrawOdds = 2.12f,
                AwayOdds = 4.55f
            };
        }

    }
}
