﻿using Frontend.Entities;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Configuration;
using System.Text;

namespace Frontend.FakeQueueProducer
{
    class FakeQueueProducer
    {
        private static readonly string UserName = ConfigurationManager.AppSettings["rabbitUsername"].ToString();
        private static readonly string Password = ConfigurationManager.AppSettings["rabbitPassword"].ToString();
        private static readonly string HostName = ConfigurationManager.AppSettings["rabbitHostName"].ToString();
        private static readonly string ExchangeName = ConfigurationManager.AppSettings["rabbitOutboundExchange"].ToString();

        private static int IdCounter = 0;

        static void Main(string[] args)
        {
            ConnectionFactory connectionFactory = new ConnectionFactory
            {
                HostName = HostName,
                UserName = UserName,
                Password = Password,

            };
            var connection = connectionFactory.CreateConnection();
            var model = connection.CreateModel();
            model.ExchangeDeclare(ExchangeName, ExchangeType.Topic, durable: true);

            var properties = model.CreateBasicProperties();

            properties.Persistent = false;

            var mockOdd1 = JsonConvert.SerializeObject(GenerateMockOdd());
            var messagebuffer =Encoding.UTF8.GetBytes(mockOdd1);
            model.BasicPublish(ExchangeName, "create", properties, messagebuffer);
            Console.WriteLine($"Published: {mockOdd1}");

            var mockOdd2 = JsonConvert.SerializeObject(GenerateMockOdd());
            messagebuffer = Encoding.UTF8.GetBytes(mockOdd2);
            model.BasicPublish(ExchangeName, "create", properties, messagebuffer);
            Console.WriteLine($"Published: {mockOdd2}");

            var updateOdd1 = JsonConvert.DeserializeObject<Odd>(mockOdd1);
            updateOdd1.AwayOdds = 1.01f;
            messagebuffer = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(updateOdd1));
            model.BasicPublish(ExchangeName, "update", properties, messagebuffer);
            Console.WriteLine($"Published: {mockOdd2}");

        }

        private static Odd GenerateMockOdd()
        {
            IdCounter++;
            return new Odd
            {
                FixtureId = $"F-{Guid.NewGuid()}",
                Name = $"Test{IdCounter} vs Test{IdCounter+1}",
                HomeOdds = 3.23f,
                DrawOdds = 2.12f,
                AwayOdds = 4.55f
            };
        }
    }
}
