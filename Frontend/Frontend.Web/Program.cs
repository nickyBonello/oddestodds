﻿using Frontend.Application;
using RabbitMQ.Client;
using System;
using System.Configuration;

namespace Frontend.Web
{
    class Program
    {
        private static readonly string UserName = ConfigurationManager.AppSettings["rabbitUsername"].ToString();
        private static readonly string Password = ConfigurationManager.AppSettings["rabbitPassword"].ToString();
        private static readonly string HostName = ConfigurationManager.AppSettings["rabbitHostName"].ToString();
        private static readonly string QueueName = ConfigurationManager.AppSettings["rabbitInboundQueue"].ToString();
        private static readonly string ExchangeName = ConfigurationManager.AppSettings["rabbitInboundExchange"].ToString();

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to OddestOdds.com");
            ConnectionFactory connectionFactory = new ConnectionFactory
            {
                HostName = HostName,
                UserName = UserName,
                Password = Password,

            };
            var connection = connectionFactory.CreateConnection();
            var channel = connection.CreateModel();
            channel.QueueDeclare(QueueName, durable: true, exclusive: false, autoDelete: false, arguments: null);
            channel.ExchangeDeclare(ExchangeName, ExchangeType.Topic, durable: true);
            channel.QueueBind(QueueName, ExchangeName, "#", null);

            // accept only one unack-ed message at a time

            // uint prefetchSize, ushort prefetchCount, bool global
            channel.BasicQos(0, 1, false);
            MessageReceiver messageReceiver = new MessageReceiver(channel);
            channel.BasicConsume(QueueName, false, messageReceiver);
            Console.ReadLine();
        }
    }
}
