﻿using System;

namespace Frontend.Entities
{
    [Serializable]
    public class Odd
    {
        public string FixtureId { get; set; }
        public string Name { get; set; }
        public float HomeOdds { get; set; }
        public float DrawOdds { get; set; }
        public float AwayOdds { get; set; }

        public Odd()
        {
        }
        public Odd(string fixtureId, string name, float homeOdds, float drawOdds, float awayOdds)
        {
            FixtureId = fixtureId;
            Name = name;
            HomeOdds = homeOdds;
            DrawOdds = drawOdds;
            AwayOdds = awayOdds;
        }

        public Odd(string name, float homeOdds, float drawOdds, float awayOdds)
        {
            FixtureId = $"F-{Guid.NewGuid()}";
            Name = name;
            HomeOdds = homeOdds;
            DrawOdds = drawOdds;
            AwayOdds = awayOdds;
        }
    }
}
