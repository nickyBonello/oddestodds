﻿using System.Text;
using RabbitMQ.Client;

namespace Frontend.Application
{
    public class MessageReceiver : DefaultBasicConsumer
    {
        private readonly IModel _channel;
        private readonly MessageHandler _messageHandler;

        public MessageReceiver(IModel channel, MessageHandler messageHandler)
        {
            _channel = channel;
            _messageHandler = messageHandler;
        }

        public MessageReceiver(IModel channel)
        {
            _channel = channel;
            _messageHandler = new MessageHandler();
        }

        public override void HandleBasicDeliver(string consumerTag, ulong deliveryTag, bool redelivered, string exchange, string routingKey, IBasicProperties properties, byte[] body)
        {
            var message = Encoding.UTF8.GetString(body).TrimEnd('\0');
            switch (routingKey)
            {
                case "create": _messageHandler.CreateOdds(message);
                        break;
                case "update": _messageHandler.UpdateOdds(message);
                    break;
                case "delete": _messageHandler.DeleteOdds();
                    break;
            }

            _channel.BasicAck(deliveryTag, false);
        }
    }
}
