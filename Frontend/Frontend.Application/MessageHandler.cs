﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Frontend.Entities;
using Newtonsoft.Json;


namespace Frontend.Application
{
    public class MessageHandler
    {
        [DllImport("kernel32")]
        static extern IntPtr GetConsoleWindow();

        public List<Odd> AllOdds = new List<Odd>();

        internal void UpdateOdds(string message)
        {
            var currentOdd = JsonConvert.DeserializeObject<Odd>(message);
            AllOdds.ForEach(f =>
            {
                if (f.FixtureId == currentOdd.FixtureId)
                {
                    f.Name = currentOdd.Name;
                    f.HomeOdds = currentOdd.HomeOdds;
                    f.DrawOdds = currentOdd.DrawOdds;
                    f.AwayOdds = currentOdd.AwayOdds;
                }
            });
            updateFrontend();
        }
        internal void CreateOdds(string message)
        {
            var currentOdd = JsonConvert.DeserializeObject<Odd>(message);

            //check if already exists
            if (AllOdds.Any(f => f.FixtureId.Equals(currentOdd.FixtureId)))
                return;

            AllOdds.Add(currentOdd);
            updateFrontend();
        }


        internal void DeleteOdds()
        {
            AllOdds.Clear();
            updateFrontend();
        }

        private void updateFrontend()
        {
            //Clearing console when doesnt exist throws exception
            if (GetConsoleWindow() != IntPtr.Zero)
                Console.Clear();

            Console.WriteLine("Welcome to OddestOdds.com");
            if (AllOdds.Count == 0)
                Console.WriteLine("Sorry! No fixtures available at this time.");
            else
                AllOdds.ForEach(f =>
                {
                    Console.WriteLine($"\n\n{f.Name}");
                    Console.WriteLine($"Home: {f.HomeOdds}");
                    Console.WriteLine($"Draw: {f.DrawOdds}");
                    Console.WriteLine($"Away: {f.AwayOdds}");
                });
        }
    }
}
