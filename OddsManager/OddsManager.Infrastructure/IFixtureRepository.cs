﻿using Frontend.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OddsManager.Infrastructure
{
    public interface IFixtureRepository
    {
        Task<IList<Odd>> loadOdds();
        void createDatabase();
        Task deleteOdds();
        Task createOdd(Odd odd);
        Task updateOdd(Odd odd);
    }
}
