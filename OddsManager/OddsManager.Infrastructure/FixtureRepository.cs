﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Frontend.Entities;

namespace OddsManager.Infrastructure
{
    public class FixtureRepository : IFixtureRepository
    {
        private string _connectionString;

        public FixtureRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void createDatabase()
        {
            using (var connection = new SqlConnection(_connectionString))
            {

                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = @"IF OBJECT_ID(N'dbo.fixtures', N'U') IS NULL BEGIN 
                    CREATE TABLE dbo.fixtures
                            (Name varchar(64) not null,
                             FixtureId varchar(64) not null,
                             HomeOdds decimal(6,3) not null,
                             DrawOdds decimal(6,3) not null,
                             AwayOdds decimal(6,3) not null); 
                    END;";
                command.ExecuteNonQuery();
            }
        }

        public async Task createOdd(Odd odd)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var command = new SqlCommand("INSERT INTO dbo.fixtures (FixtureId, Name, HomeOdds, DrawOdds, AwayOdds) " +
                    "VALUES (@FixtureId, @Name, @HomeOdds, @DrawOdds, @AwayOdds)", connection);

                command.Parameters.AddWithValue("@FixtureId", odd.FixtureId);
                command.Parameters.AddWithValue("@Name", odd.Name);
                command.Parameters.AddWithValue("@HomeOdds", odd.HomeOdds);
                command.Parameters.AddWithValue("@DrawOdds", odd.DrawOdds);
                command.Parameters.AddWithValue("@AwayOdds", odd.AwayOdds);

                await command.ExecuteNonQueryAsync();
            }
        }

        public async Task updateOdd(Odd odd)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var pFixtureId = new SqlParameter("@FixtureId", System.Data.SqlDbType.VarChar, 64)
                {
                    Value = odd.FixtureId
                };
                var pName = new SqlParameter("@Name", System.Data.SqlDbType.VarChar, 64)
                {
                    Value = odd.Name
                };
                var pHomeOdds = new SqlParameter("@HomeOdds", System.Data.SqlDbType.Decimal)
                {
                    Value = odd.HomeOdds
                };
                var pDrawOdds = new SqlParameter("@DrawOdds", System.Data.SqlDbType.Decimal)
                {
                    Value = odd.DrawOdds
                };
                var pAwayOdds = new SqlParameter("@AwayOdds", System.Data.SqlDbType.Decimal)
                {
                    Value = odd.AwayOdds
                };

                var command = new SqlCommand("UPDATE dbo.fixtures " +
                    "SET Name = @Name, HomeOdds = @HomeOdds, DrawOdds = @DrawOdds, AwayOdds = @AwayOdds " +
                    "Where FixtureId = @FixtureId", connection);


                command.Parameters.AddWithValue("@FixtureId", odd.FixtureId);
                command.Parameters.AddWithValue("@Name", odd.Name);
                command.Parameters.AddWithValue("@HomeOdds", odd.HomeOdds);
                command.Parameters.AddWithValue("@DrawOdds", odd.DrawOdds);
                command.Parameters.AddWithValue("@AwayOdds", odd.AwayOdds);

                await command.ExecuteNonQueryAsync();
            }
        }

        public async Task deleteOdds()
        {
            using (var connection = new SqlConnection(_connectionString))
            {

                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "DELETE FROM dbo.fixtures";
                await command.ExecuteNonQueryAsync();
            }
        }

        public async Task<IList<Odd>> loadOdds()
        {
            var oddsList = new List<Odd>();

            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var query = "SELECT * FROM dbo.fixtures";
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    using (SqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        while (reader.Read())
                        {
                            var odd = new Odd();
                            odd.FixtureId = reader["FixtureId"].ToString();
                            odd.Name = reader["Name"].ToString();
                            odd.HomeOdds = float.Parse(reader["HomeOdds"].ToString());
                            odd.DrawOdds = float.Parse(reader["DrawOdds"].ToString());
                            odd.AwayOdds = float.Parse(reader["AwayOdds"].ToString());

                            oddsList.Add(odd);
                        }
                    }
                }
            }

            return oddsList;
        }
    }
}
