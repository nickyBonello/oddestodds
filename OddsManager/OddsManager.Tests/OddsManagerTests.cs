﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Frontend.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using OddsManager.Application;
using OddsManager.Infrastructure;
using RabbitMQ.Client;

namespace OddsManager.Tests
{
    [TestClass]
    public class OddsManagerTests
    {

        [TestMethod]
        public async Task NoFixturesInDB()
        {
            var ifixtureRepo = new Mock<IFixtureRepository>();
            var imessagePublisher = new Mock<MessagePublisher>(It.IsAny<IModel>(), It.IsAny<string>());
            var actionHandler = new Mock<ActionHandler>(ifixtureRepo.Object, It.IsAny<MessagePublisher>());

            ifixtureRepo.Setup(f => f.loadOdds()).ReturnsAsync(new List<Odd>() {});

            var output = await actionHandler.Object.loadOdds("create");
            Assert.IsTrue(output.Count == 0);
        }

        [TestMethod]
        public async Task FixturesInDB()
        {
            var ifixtureRepo = new Mock<IFixtureRepository>();
            var imessagePublisher = new Mock<IMessagePublisher>();
            var actionHandler = new Mock<ActionHandler>(ifixtureRepo.Object, imessagePublisher.Object);

            ifixtureRepo.Setup(f => f.loadOdds()).ReturnsAsync(new List<Odd>() { GenerateMockOdd() });
            imessagePublisher.Setup(m => m.displayOdds(It.IsAny<IList<Odd>>(),It.IsAny<string>())).Verifiable();

            var output = await actionHandler.Object.loadOdds("create");
            Assert.IsTrue(output.Count == 1);
        }

        [TestMethod]
        public async Task DeleteAll()
        {
            var ifixtureRepo = new Mock<IFixtureRepository>();
            var imessagePublisher = new Mock<IMessagePublisher>();
            var actionHandler = new Mock<ActionHandler>(ifixtureRepo.Object, imessagePublisher.Object);

            ifixtureRepo.Setup(f => f.loadOdds()).ReturnsAsync(new List<Odd>() { GenerateMockOdd() });

            var output = await actionHandler.Object.loadOdds("create");
            Assert.IsTrue(output.Count == 1);

            var deleteOutput = await actionHandler.Object.deleteOdds();
            Assert.IsTrue(deleteOutput.Odd == null);
            Assert.IsTrue(deleteOutput.RoutingKey == "delete");
        }

        [TestMethod]
        public async Task DeleteWhenAlreadyEmpty()
        {
            var ifixtureRepo = new Mock<IFixtureRepository>();
            var imessagePublisher = new Mock<IMessagePublisher>();
            var actionHandler = new Mock<ActionHandler>(ifixtureRepo.Object, imessagePublisher.Object);

            var output = await actionHandler.Object.deleteOdds();
            Assert.IsTrue(output.Odd == null);
            Assert.IsTrue(output.RoutingKey == "delete");
        }

        [TestMethod]
        public async Task CreateNewFixture()
        {
            var ifixtureRepo = new Mock<IFixtureRepository>();
            var imessagePublisher = new Mock<IMessagePublisher>();
            var actionHandler = new Mock<ActionHandler>(ifixtureRepo.Object, imessagePublisher.Object);

            var odd = GenerateMockOdd();
            ifixtureRepo.Setup(f => f.loadOdds()).ReturnsAsync(new List<Odd>()
            { odd }).Verifiable();

            var output = await actionHandler.Object.createOdd(odd);
            Assert.IsTrue(output.Odd.FixtureId == odd.FixtureId);
            Assert.IsTrue(output.RoutingKey == "create");
        }

        [TestMethod]
        public async Task UpdateExistingFixture()
        {
            var ifixtureRepo = new Mock<IFixtureRepository>();
            var imessagePublisher = new Mock<IMessagePublisher>();
            var actionHandler = new Mock<ActionHandler>(ifixtureRepo.Object, imessagePublisher.Object);

            var odd = GenerateMockOdd();
            ifixtureRepo.Setup(f => f.loadOdds()).ReturnsAsync(new List<Odd>()
                { odd }).Verifiable();

            var firstHomeOdds = odd.HomeOdds;

            var output = await actionHandler.Object.createOdd(odd);
            Assert.IsTrue(output.Odd.FixtureId == odd.FixtureId);
            Assert.IsTrue(output.RoutingKey == "create");
            Assert.AreEqual(firstHomeOdds, odd.HomeOdds);

            odd.HomeOdds = 6.66f;
            output = await actionHandler.Object.updateOdd(odd);

            Assert.IsTrue(output.Odd.FixtureId == odd.FixtureId);
            Assert.IsTrue(output.RoutingKey == "update");
            Assert.AreNotEqual(firstHomeOdds, odd.HomeOdds);
        }


        private static Odd GenerateMockOdd()
        {
            return new Odd
            {
                FixtureId = $"F-{Guid.NewGuid()}",
                Name = $"Test1 vs Test2",
                HomeOdds = 3.23f,
                DrawOdds = 2.12f,
                AwayOdds = 4.55f
            };
        }
    }
}
