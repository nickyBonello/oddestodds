﻿using Frontend.Entities;
using OddsManager.Application;
using OddsManager.Entities;
using OddsManager.Infrastructure;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace OddsManager
{
    class Program
    {
        private static readonly string UserName = ConfigurationManager.AppSettings["rabbitUsername"].ToString();
        private static readonly string Password = ConfigurationManager.AppSettings["rabbitPassword"].ToString();
        private static readonly string HostName = ConfigurationManager.AppSettings["rabbitHostName"].ToString();
        private static readonly string ExchangeName = ConfigurationManager.AppSettings["rabbitInboundExchange"].ToString();

        private static readonly string connectionString = ConfigurationManager.ConnectionStrings["OddestOddsDB"].ConnectionString;

        private static readonly FixtureRepository _fixtureRepository = new FixtureRepository(connectionString);
        private static ActionHandler _actionHandler;
        private static MessagePublisher _messagePublisher;

        private static List<Odd> fixtureList = new List<Odd>();
        private static List<RabbitMessage> messageList = new List<RabbitMessage>();



        static void Main(string[] args)
        {
            startup().Wait();

            var stop = false;
            while (!stop)
            {
                Console.WriteLine("OddestOdds.com BackendService\n\n");


                try
                {
                    Console.WriteLine("Available fixtures: \n");
                    if (fixtureList.Count == 0)
                        Console.WriteLine("Sorry! No fixtures available at this time.");
                    else
                        fixtureList.ForEach(f =>
                        {
                            Console.WriteLine($"FixtureID: {f.FixtureId}\n" +
                                $"Fixture: {f.Name}\n" +
                                $"Home: {f.HomeOdds}\n" +
                                $"Draw: {f.DrawOdds}\n" +
                                $"Away: {f.AwayOdds}\n");                   
                        });
                }
                catch(Exception ex)
                {
                    Console.WriteLine($"Error when loading odds: {ex}");
                }
                Console.WriteLine("\nPlease Select an option:" +
                    "\n1. Create new fixture" +
                    "\n2. Update existing fixture odds" +
                    "\n3. Delete all fixtures" +
                    "\n4. Publish current odds to frontend\n");
                var input = Console.ReadLine();

                switch (input)
                {
                    //createNew
                    case "1":
                        handleCreate();
                        break;
                    //update
                    case "2":
                        handleUpdate();
                        break;
                    //delete
                    case "3":
                        var message = _actionHandler.deleteOdds().Result;
                        messageList.Clear();
                        fixtureList.Clear();
                        messageList.Add(message);
                        break;
                    //publish
                    case "4":
                        _messagePublisher.publishMessages(messageList);
                        break;
                    default:
                        break;
                }

                if (input == "q")
                {
                    Console.WriteLine("Goodbye!");
                    stop = true;
                }

                else
                {
                    Console.WriteLine("Your birthday did not match our records. Please try again");
                    Console.Clear();
                }
            }
        }

        static void handleCreate()
        {
            Console.WriteLine("Enter Fixture Name:");
            var fixtureName = Console.ReadLine();

            Console.WriteLine("Enter Home Odds:");
            if (!float.TryParse(Console.ReadLine(), out float homeOdds))
            {
                Console.Clear();
                Console.WriteLine("\n\nERROR! Not a valid float, please try again.\n\n");
            }
            Console.WriteLine("Enter Draw Odds:");
            if (!float.TryParse(Console.ReadLine(), out float drawOdds))
            {
                Console.Clear();
                Console.WriteLine("\n\nERROR! Not a valid float, please try again.\n\n");
            }

            Console.WriteLine("Enter Away Odds:");
            if (!float.TryParse(Console.ReadLine(), out float awayOdds))
            {
                Console.Clear();
                Console.WriteLine("\n\nERROR! Not a valid float, please try again.\n\n");
            }

            var message = _actionHandler.createOdd(new Odd(fixtureName, homeOdds, drawOdds, awayOdds)).Result;
            messageList.Add(message);
            fixtureList.Add(message.Odd);
        }

        static void handleUpdate()
        {
            Console.WriteLine("Enter FixtureId to update:");
            var fixtureId = Console.ReadLine();

            if(!fixtureList.Any(o => o.FixtureId.Equals(fixtureId)))
            {
                Console.Clear();
                Console.WriteLine("No fixture with given fixtureID found!");
            }

            Console.WriteLine("Enter Fixture Name:");
            var fixtureName = Console.ReadLine();

            Console.WriteLine("Enter Home Odds:");
            if (!float.TryParse(Console.ReadLine(), out float homeOdds))
            {
                Console.Clear();
                Console.WriteLine("\n\nERROR! Not a valid float, please try again.\n\n");
            }
            Console.WriteLine("Enter Draw Odds:");
            if (!float.TryParse(Console.ReadLine(), out float drawOdds))
            {
                Console.Clear();
                Console.WriteLine("\n\nERROR! Not a valid float, please try again.\n\n");
            }

            Console.WriteLine("Enter Away Odds:");
            if (!float.TryParse(Console.ReadLine(), out float awayOdds))
            {
                Console.Clear();
                Console.WriteLine("\n\nERROR! Not a valid float, please try again.\n\n");
            }

            var message = _actionHandler.updateOdd(new Odd(fixtureId, fixtureName, homeOdds, drawOdds, awayOdds)).Result;
            messageList.Add(message);
            fixtureList.ForEach(f =>
            {
                if (f.FixtureId == fixtureId)
                {
                    f.Name = fixtureName;
                    f.HomeOdds = homeOdds;
                    f.DrawOdds = drawOdds;
                    f.AwayOdds = awayOdds;
                }
            });
        }

        static async Task startup()
        {
            ConnectionFactory connectionFactory = new ConnectionFactory
            {
                HostName = HostName,
                UserName = UserName,
                Password = Password,
            };

            var connection = connectionFactory.CreateConnection();
            var model = connection.CreateModel();

            _messagePublisher = new MessagePublisher(model, ExchangeName);
            _actionHandler = new ActionHandler(_fixtureRepository, _messagePublisher);

            model.ExchangeDeclare(ExchangeName, ExchangeType.Topic, durable: true);

            var properties = model.CreateBasicProperties();

            properties.Persistent = false;

            _actionHandler.createDatabase();
            var messages = await _actionHandler.loadOdds("create");
            messages.ToList().ForEach(m => {
                messageList.Add(m);
                fixtureList.Add(m.Odd);
                });
        }
    }
}
