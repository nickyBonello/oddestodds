﻿using Frontend.Entities;
using OddsManager.Entities;
using System.Collections.Generic;

namespace OddsManager.Application
{
    public interface IMessagePublisher
    {
        void displayOdds(IList<Odd> odds, string routingKey);
        void deleteOdds();
        void publishMessages(IList<RabbitMessage> messages);
    }
}
