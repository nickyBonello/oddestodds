﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Frontend.Entities;
using OddsManager.Entities;
using OddsManager.Infrastructure;

namespace OddsManager.Application
{
    public class ActionHandler : IActionHandler
    {
        private readonly IFixtureRepository _fixtureRepository;
        private readonly IMessagePublisher _messagePublisher;

        public ActionHandler(IFixtureRepository fixtureRepo, IMessagePublisher messagePublisher)
        {
            _fixtureRepository = fixtureRepo;
            _messagePublisher = messagePublisher;
        }

        public void createDatabase()
        {
            _fixtureRepository.createDatabase();
        }

        public async Task<RabbitMessage> createOdd(Odd odd)
        {
            await _fixtureRepository.createOdd(odd);

            var updatedList = await _fixtureRepository.loadOdds();
            if (updatedList.Any(o => o.FixtureId == odd.FixtureId))
                //_messagePublisher.displayOdds(new List<Odd> { odd }, "create");
                return new RabbitMessage(odd, "create");

            return null;
        }

        public async Task<RabbitMessage> updateOdd(Odd odd)
        {
            await _fixtureRepository.updateOdd(odd);

            var updatedList = (await _fixtureRepository.loadOdds()).ToList();
            if (updatedList.Any(o => o.FixtureId == odd.FixtureId))
                //_messagePublisher.displayOdds(new List<Odd> { odd }, "update");
                return new RabbitMessage(odd, "update");

            return null;
        }

        public async Task<RabbitMessage> deleteOdds()
        {
            await _fixtureRepository.deleteOdds();
            //_messagePublisher.deleteOdds();

            return new RabbitMessage(null, "delete");
        }

        public async Task<IList<RabbitMessage>> loadOdds(string routingKey)
        {
            var odds = (await _fixtureRepository.loadOdds()).ToList();
            //if(odds.Count != 0)
              //  _messagePublisher.displayOdds(odds, routingKey);

            return odds.Select(o => new RabbitMessage(o, "create")).ToList();
            
        }
    }
}
