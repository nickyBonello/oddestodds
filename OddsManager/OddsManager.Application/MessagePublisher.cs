﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Frontend.Entities;
using Newtonsoft.Json;
using OddsManager.Entities;
using RabbitMQ.Client;

namespace OddsManager.Application
{
    public class MessagePublisher : IMessagePublisher
    {
        private readonly IModel _channel;
        private readonly string _exchangeName;

        public MessagePublisher(IModel model, string exchangeName)
        {
            _channel = model;
            _exchangeName = exchangeName;

        }

        public void deleteOdds()
        {
            publish(null, "delete");
        }

        public void displayOdds(IList<Odd> odds, string routingKey)
        {
            odds.ToList().ForEach(o =>
            {
                publish(o, routingKey);
            });
        }

        public void publishMessages(IList<RabbitMessage> messages)
        {
            messages.ToList().ForEach(m =>
            {
                publish(m.Odd, m.RoutingKey);
            });
        }

        internal void publish(Odd ? odd, string routingKey)
        {
            var jsonOdd = odd == null? "{}" : JsonConvert.SerializeObject(odd);
            var properties = _channel.CreateBasicProperties();
            properties.Persistent = false;

            var messagebuffer = Encoding.UTF8.GetBytes(jsonOdd);

            _channel.BasicPublish(_exchangeName, routingKey, properties, messagebuffer);
        }
    }
}
