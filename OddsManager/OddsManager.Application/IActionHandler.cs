﻿using Frontend.Entities;
using OddsManager.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OddsManager.Application
{
    interface IActionHandler
    {
        void createDatabase();
        Task<IList<RabbitMessage>> loadOdds(string routingKey);
        Task<RabbitMessage> createOdd(Odd odd);
        Task<RabbitMessage> updateOdd(Odd odd);
        Task<RabbitMessage> deleteOdds();
    }
}
