﻿using Frontend.Entities;
using System;

namespace OddsManager.Entities
{
    public class RabbitMessage
    {
        public Odd Odd { get; set; }
        public string RoutingKey { get; set; }

        public RabbitMessage(Odd odd, string routingKey)
        {
            Odd = odd;
            RoutingKey = routingKey;
        }
    }
}
